package com.example.Wallet.Controller;

import com.example.Wallet.Model.Wallet;
import com.example.Wallet.Services.WalletServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/wallet/")
public class WalletController {

    @Autowired
    WalletServices walletServices;

    @GetMapping
    public List<Wallet> getWallet() {
        return walletServices.listAllWallet();
    }

    @GetMapping("{id}")
    public Optional<Wallet> getWalletById(@PathVariable("id") Long id) {
        return walletServices.listWalletById(id);
    }

    @PostMapping
    public String createWallet(@RequestBody Wallet wallet) {
        walletServices.createWallet(wallet);
        return "Wallet Created Successfully";
    }

    @DeleteMapping("{id}")
    public String deleteWallet(@PathVariable("id") Long id) {
        walletServices.delete(id);
        return "Wallet deleted : id - "+id ;
    }

}
