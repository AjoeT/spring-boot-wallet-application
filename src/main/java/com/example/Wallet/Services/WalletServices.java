package com.example.Wallet.Services;

import com.example.Wallet.Model.Wallet;
import com.example.Wallet.Repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WalletServices {

    @Autowired
    WalletRepository walletRepository;

    public  void createWallet(Wallet wallet) {
        walletRepository.save(wallet);
    }

    public List<Wallet> listAllWallet() {
        return walletRepository.findAll();
    }

    public Optional<Wallet> listWalletById(Long id) {
        return walletRepository.findById(id);
    }

    public void update (Wallet updatedWallet) {
        Optional<Wallet> walletToBeUpdated = walletRepository.findById(updatedWallet.getId());
        walletToBeUpdated.get().setUserName(updatedWallet.getUserName());
        walletToBeUpdated.get().setBalance(updatedWallet.getBalance());
        walletRepository.save(walletToBeUpdated.get());
    }

    public void delete (Long id) {
        walletRepository.deleteById(id);
    }




}
