//This is the model

package com.example.Wallet.Model;

import javax.persistence.*;


@Entity
@Table(name="wallet")
public class Wallet {

  @Id
  @GeneratedValue
  @Column(name="id")
  Long id;

  @Column(name="username")
  String userName;

  @Column(name="balance")
  float balance;

    public Long getId() {
        return id;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
